# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.2.4

- patch: Internal maintenance: Bump base docker image to 2.70.0.
- patch: Internal maintenance: Update pipes versions in pipelines configuration file.

## 1.2.3

- patch: Internal maintenance: Bump pipes versions in pipelines configuration file.
- patch: Internal maintenance: Update docker image to azure-cli:2.67.0

## 1.2.2

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 1.2.1

- patch: Fix README example.

## 1.2.0

- minor: Add feature to support AZURE_SUBSCRIPTION selection when having multiple subscriptions on service principal.
- minor: Internal maintenance: Update pipe's docker image to mcr.microsoft.com/azure-cli:2.60.0.
- patch: Internal maintenance: Update pipes versions in pipelines configuration file.

## 1.1.1

- patch: Update the readme with detailed example.

## 1.1.0

- minor: Add feature to change default Azure cloud to government cloud.
- patch: Internal maintenance: update azure-cli image and bitbucket-pipes-toolkit-bash.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update default image, pipe versions in bitbucket-pipelines.yml.

## 1.0.1

- patch: Internal maintenance: make new pipe consistent to others.

## 1.0.0

- major: Note: This pipe was forked from https://bitbucket.org/microsoft/azure-web-apps-deploy for future maintenance purposes.

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Switch naming conventions from task to pipes.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines Azure Web Apps deployment pipe.
