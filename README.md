# Bitbucket Pipelines Pipe: Azure Web Apps Deploy

Deploys an application to [Azure Web Apps](https://azure.microsoft.com/en-gb/services/app-service/web). Azure Web Apps enables you to build and host web applications in the programming language of your choice without managing infrastructure. It offers auto-scaling and high availability.

Note: This pipe was forked from https://bitbucket.org/microsoft/azure-web-apps-deploy for future maintenance purposes.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
    - pipe: atlassian/azure-web-apps-deploy:1.2.4
      variables:
        AZURE_APP_ID: $AZURE_APP_ID
        AZURE_PASSWORD: $AZURE_PASSWORD
        AZURE_TENANT_ID: $AZURE_TENANT_ID
        AZURE_RESOURCE_GROUP: '<string>'
        AZURE_APP_NAME: '<string>'
        ZIP_FILE: '<string>'
        # AZURE_SUBSCRIPTION: '<string>' # Optional.
        # SLOT: '<string>' # Optional.
        # AZURE_CLOUD_ENVIRONMENT: '<string>' # Optional
        # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable                 | Usage                                                                                                                                                                                     |
|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| AZURE_APP_ID (*)         | The app ID, URL or name associated with the service principal required for login.                                                                                                         |
| AZURE_PASSWORD (*)       | Credentials like the service principal password, or path to certificate required for login.                                                                                               |
| AZURE_TENANT_ID  (*)     | The AAD tenant required for login with the service principal.                                                                                                                             |
| AZURE_RESOURCE_GROUP (*) | Name of the resource group that the app service is deployed to.                                                                                                                           |
| AZURE_APP_NAME (*)       | Name of the web app you want to deploy.                                                                                                                                                   |
| ZIP_FILE (*)             | Zip file path for deployment e.g. app.zip                                                                                                                                                 |
| AZURE_SUBSCRIPTION       | Name or UUID of the Azure subscription to set before login to Azure in the case if current service principal has multiple subscriptions. By default subscription is not set before login. |
| SLOT                     | Name of the slot. Defaults to the production slot if not specified.                                                                                                                       |
| AZURE_CLOUD_ENVIRONMENT  | Allow deployment to non-default Azure cloud i.e Azure government clouds.                                                                                                                  |
| DEBUG                    | Turn on extra debug information. Default: `false`.                                                                                                                                        |

_(*) = required variable._

## Prerequisites

You will need to configure required Azure resources before running the pipe. The easiest way to do it is by using the Azure cli. You can either [install the Azure cli](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest) on your local machine, or you can use the [Azure Cloud Shell](https://docs.microsoft.com/en-us/azure/cloud-shell/overview) provided by the Azure Portal in a browser.

### Service principal

You will need a service principal with sufficient access to create an Azure App Service instance, or update an existing App Service. To create a service principal using the Azure CLI, execute the following command in a bash shell:

```sh
az ad sp create-for-rbac --name MyServicePrincipal
```

Refer to the following documentation for more detail:

* [Create an Azure service principal with Azure CLI](https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli)

### App Service

Using the service principal credentials obtained in the previous step, you can use the following commands to create an Azure App Service instance in a Bash shell:

```bash
az login --service-principal --username ${AZURE_APP_ID}  --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}

az group create --name ${AZURE_RESOURCE_GROUP} --location australiaeast

az appservice plan create --name ${AZURE_APP_NAME} --resource-group ${AZURE_RESOURCE_GROUP} --sku FREE

az webapp create --name ${AZURE_APP_NAME} --resource-group ${AZURE_RESOURCE_GROUP} --plan $AZURE_APP_NAME
```

Note!
If you want to deploy to Azure government cloud take a look into some difference for Web App between default and government clouds:
* [Difference for Web App between default and government clouds](https://docs.microsoft.com/en-us/azure/azure-government/compare-azure-government-global-azure#app-service)

Refer to the following documentation for more detail:

* [Create an App Service app and deploy code from a local Git repository using Azure CLI](https://docs.microsoft.com/en-us/azure/app-service/scripts/cli-deploy-local-git)

## Examples

### Basic example

Build zip archive and deploy application to Azure Web Apps
```yaml
- step:
    name: "Build and test"
    script:
      - npm install
      - npm test
      - zip -r my-package-$BITBUCKET_BUILD_NUMBER.zip .
    artifacts:
      - my-package-*.zip
- step:
    name: "Deploy to Azure"
    script:
      - pipe: atlassian/azure-web-apps-deploy:1.2.4
        variables:
          AZURE_APP_ID: $AZURE_APP_ID
          AZURE_PASSWORD: $AZURE_PASSWORD
          AZURE_TENANT_ID: $AZURE_TENANT_ID
          AZURE_RESOURCE_GROUP: $AZURE_RESOURCE_GROUP
          AZURE_APP_NAME: 'my-site'
          ZIP_FILE: 'my-package-$BITBUCKET_BUILD_NUMBER.zip'
```

### Advanced example

```yaml
script:
  - pipe: atlassian/azure-web-apps-deploy:1.2.4
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_RESOURCE_GROUP: $AZURE_RESOURCE_GROUP
      AZURE_APP_NAME: 'my-site'
      ZIP_FILE: 'my-package.zip'
      SLOT: 'staging'
```

If service principal used in the pipe has multiple subscriptions, you can set precise subscription to be able to login properly:

```yaml
script:
  - pipe: atlassian/azure-web-apps-deploy:1.2.4
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_RESOURCE_GROUP: $AZURE_RESOURCE_GROUP
      AZURE_APP_NAME: 'my-site'
      ZIP_FILE: 'my-package-$BITBUCKET_BUILD_NUMBER.zip'
      AZURE_SUBSCRIPTION: 'My Azure Subscription'
```

deploy to government cloud:
```yaml
script:
  - pipe: atlassian/azure-web-apps-deploy:1.2.4
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_RESOURCE_GROUP: $AZURE_RESOURCE_GROUP
      AZURE_APP_NAME: 'my-site'
      ZIP_FILE: 'my-package.zip'
      AZURE_CLOUD_ENVIRONMENT: 'AzureUSGovernment'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,azure,web-apps
